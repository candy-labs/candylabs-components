import React from 'react'
import classNames from 'classnames/bind'

import layout from './layout.component.scss'

export const Wrapper = props => (
    <div {...props} className={layout.wrapper} />
)
export const Content = props => (
    <div {...props} className={layout.content} />
)
