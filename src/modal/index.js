import React from 'react'
import modal from './modal.component.scss'
import classNames from 'classnames/bind'

import Close from '../assets/Close'

const cssModal = classNames.bind(modal)

export default props => {
    let { active, handleClick } = props
    return (
        <div
            className={cssModal({
                modal: true,
                active,
            })}>
            <div
                className={cssModal({
                    overlay: true,
                })}
                onClick={handleClick}
            />
            <div
                className={cssModal({
                    container: true,
                })}>
                <button
                    className={cssModal({
                        close: true,
                    })}
                    onClick={handleClick}>
                    <Close />
                </button>
                {props.children}
            </div>
        </div>
    )
}
