import React from 'react'
import button from './button.component.scss'
import link from './link.component.scss'
import classNames from 'classnames/bind'

const cssButton = classNames.bind(button)

export const Link = props => (
    <a
        {...props}
        className={props.unstyled ? link.unstyledLink : link.link}>
        <span>{props.children}</span>
    </a>
)

export default props => {
    let className = cssButton({
        button: true,
        primary: props.primary,
        secondary: props.secondary,
        light: props.light,
        inverted: props.inverted,
    })

    return <button {...props} className={className} />
}
