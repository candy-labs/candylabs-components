import React from 'react'
import code from './code.component.scss'
import classNames from 'classnames/bind'

import { PrismLight as SyntaxHighlighter } from 'react-syntax-highlighter'
import jsx from 'react-syntax-highlighter/dist/languages/prism/jsx'
import javascript from 'react-syntax-highlighter/dist/languages/prism/javascript'
import python from 'react-syntax-highlighter/dist/languages/prism/python'
import darcula from 'react-syntax-highlighter/dist/styles/prism/darcula'

const cssCode = classNames.bind(code)

SyntaxHighlighter.registerLanguage('jsx', jsx)
SyntaxHighlighter.registerLanguage('javascript', javascript)
SyntaxHighlighter.registerLanguage('python', python)

export default props => {
    return (
        <SyntaxHighlighter
            className={cssCode({ code: true })}
            language={props.language}
            showLineNumbers="true"
            wrapLines="true"
            style={darcula}>
            {props.children}
        </SyntaxHighlighter>
    )
}
