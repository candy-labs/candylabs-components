import React from 'react'
import form from './form.component.scss'
import classNames from 'classnames/bind'

const cssForm = classNames.bind(form)

export const Form = props => (
    <form {...props} className={`${props.className}`} />
)
export const Input = props => <input {...props} />
export const Textarea = props => <textarea {...props} />
export const Checkbox = props => {
    let className = cssForm({
        form: false,
        smaller: props.smaller,
        smallest: props.smallest,
    })
    return (
        <div
            style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
            }}>
            <input
                type="checkbox"
                name={props.name}
                id={props.id}
                checked={props.value}
                onChange={props.onChange}
                required
            />
            <label htmlFor={props.id} className={className}>
                {props.children}
            </label>
        </div>
    )
}
