import React from 'react'
import classNames from 'classnames/bind'

import footer from './footer.component.scss'

import ClLogoWhite from '../assets/ClLogoWhite'
import Facebook from '../assets/Facebook'
import Twitter from '../assets/Twitter'
import Instagram from '../assets/Instagram'
import LinkedIn from '../assets/LinkedIn'

const cssFooter = classNames.bind(footer)

export default props => {
    return (
        <footer className={cssFooter({ footer: true })}>
            <div className={cssFooter({ end: true })}>
                <div className={cssFooter({ endLeft: true })}>
                    <a
                        href="https://www.candylabs.de/"
                        target="_blank"
                        className={cssFooter({ logoLink: true })}>
                        <ClLogoWhite />
                    </a>
                </div>
                <div className={cssFooter({ endMiddle: true })}>
                    <a
                        href="https://www.candylabs.de/datenschutz"
                        target="_blank">
                        Datenschutz
                    </a>
                    <a
                        href="https://www.candylabs.de/impressum"
                        target="_blank">
                        Impressum
                    </a>
                </div>
                <div className={cssFooter({ endRight: true })}>
                    <a
                        rel="noopener"
                        target="_blank"
                        href="https://facebook.com/candylabsHQ">
                        <Facebook />
                    </a>
                    <a
                        rel="noopener"
                        target="_blank"
                        href="https://twitter.com/candylabsHQ">
                        <Twitter />
                    </a>
                    <a
                        rel="noopener"
                        target="_blank"
                        href="https://instagram.com/candylabsHQ">
                        <Instagram />
                    </a>
                    <a
                        rel="noopener"
                        target="_blank"
                        href="https://de.linkedin.com/company/candylabs">
                        <LinkedIn />
                    </a>
                </div>
            </div>
        </footer>
    )
}
