import React, { Component } from 'react'
import Recaptcha from 'react-recaptcha'
import classNames from 'classnames/bind'

import textIconTeaser from './textIconTeaser.component.scss'
import clients from './clients.component.scss'
import contact from './contact.component.scss'

import Button from '../button'

import Mail from '../assets/Mail'
import Telephone from '../assets/Telephone'
import DanielPutsche from './assets/daniel-putsche.jpg'

const cssTextIconTeaser = classNames.bind(textIconTeaser)
const cssClients = classNames.bind(clients)
const cssContact = classNames.bind(contact)
z
export const TextIconTeaser = props => (
    <div className={cssTextIconTeaser({ textIconTeaser: true })}>
        {props
            ? props.items.map(item => {
                  return (
                      <div key={item.title}>
                          <div className={cssTextIconTeaser({ icon: true })}>
                              {item.icon}
                          </div>
                          <div className={cssTextIconTeaser({ text: true })}>
                              <h3>{item.title}</h3>
                              <p>{item.content}</p>
                          </div>
                      </div>
                  )
              })
            : ''}
    </div>
)

export const Clients = props => (
    <div className={cssClients({ clients: true })}>
        <div className={cssClients({ title: true })}>
            <h2>{props.title}</h2>
            <p>{props.subtitle}</p>
        </div>
        <div className={cssClients({ content: true })}>
            <div className={cssClients({ logos: true })}>
                {props.items.map(item => {
                    return <div key={item.id}>{item.logo}</div>
                })}
            </div>
        </div>
    </div>
)

export class Contact extends Component {
    state = {
        disabled: true,
        data: {
            firstname: '',
            lastname: '',
            email: '',
            message: '',
            consent: false,
        },
    }
    render() {
        return (
            <div className={cssContact({ contact: true })}>
                <div className={cssContact({ wrapper: true })}>
                    <div className={cssContact({ title: true })}>
                        <h2>Mehr Themen zu Digitalisierung?</h2>
                        <p>
                            Haben Sie weiteren Bedarf im Bereich
                            Digitalisierung und benötigen Unterstützung?
                            <br />
                            Sprechen Sie uns gerne direkt an.
                        </p>
                    </div>

                    <div
                        className={cssContact({ content: true })}
                        data-aos="fade-up"
                        data-aos-anchor=".cl-contactcard .title">
                        <div className={cssContact({ form: true })}>
                            <form
                                method="post"
                                className={cssContact({
                                    'cl-form-contact': true,
                                })}
                                onSubmit={e => {
                                    e.preventDefault()
                                    this.props.onSubmit(this.state.data)
                                    this.setState({
                                        disabled: true,
                                        data: {
                                            firstname: '',
                                            lastname: '',
                                            email: '',
                                            message: '',
                                            consent: false,
                                        },
                                    })
                                }}
                                action="">
                                <label title="Vorname">
                                    <input
                                        type="text"
                                        name="firstname"
                                        autoComplete="given-name"
                                        placeholder="Vorname"
                                        value={this.state.data.firstname}
                                        onChange={e => {
                                            let data = {
                                                ...this.state.data,
                                                firstname: e.target.value,
                                            }
                                            let disabled =
                                                data.firstname &&
                                                data.lastname &&
                                                data.email &&
                                                data.message &&
                                                data.consent
                                                    ? false
                                                    : true
                                            this.setState({ disabled, data })
                                        }}
                                        required
                                    />
                                </label>
                                <label title="Name">
                                    <input
                                        type="text"
                                        name="lastname"
                                        autoComplete="family-name"
                                        placeholder="Name"
                                        value={this.state.data.lastname}
                                        onChange={e => {
                                            let data = {
                                                ...this.state.data,
                                                lastname: e.target.value,
                                            }
                                            let disabled =
                                                data.firstname &&
                                                data.lastname &&
                                                data.email &&
                                                data.message &&
                                                data.consent
                                                    ? false
                                                    : true
                                            this.setState({ disabled, data })
                                        }}
                                        required
                                    />
                                </label>
                                <label
                                    title="E-Mail Adresse"
                                    className={cssContact({ large: true })}>
                                    <input
                                        type="email"
                                        name="email"
                                        autoComplete="email"
                                        placeholder="E-Mail Adresse"
                                        value={this.state.data.email}
                                        onChange={e => {
                                            let data = {
                                                ...this.state.data,
                                                email: e.target.value,
                                            }
                                            let disabled =
                                                data.firstname &&
                                                data.lastname &&
                                                data.email &&
                                                data.message &&
                                                data.consent
                                                    ? false
                                                    : true
                                            this.setState({ disabled, data })
                                        }}
                                        required
                                    />
                                </label>

                                <label
                                    title="Ihre Nachricht"
                                    className={cssContact({ large: true })}>
                                    <textarea
                                        name="message"
                                        placeholder="Ihre Nachricht"
                                        required
                                        value={this.state.data.message}
                                        onChange={e => {
                                            let data = {
                                                ...this.state.data,
                                                message: e.target.value,
                                            }
                                            let disabled =
                                                data.firstname &&
                                                data.lastname &&
                                                data.email &&
                                                data.message &&
                                                data.consent
                                                    ? false
                                                    : true
                                            this.setState({ disabled, data })
                                        }}
                                    />
                                </label>

                                <div className={cssContact({ large: true })}>
                                    <input
                                        type="checkbox"
                                        name="terms_accepted"
                                        checked={this.state.data.consent}
                                        id="terms_accepted"
                                        onChange={e => {
                                            let data = {
                                                ...this.state.data,
                                                consent: e.target.checked,
                                            }
                                            let disabled =
                                                data.firstname &&
                                                data.lastname &&
                                                data.email &&
                                                data.message &&
                                                data.consent
                                                    ? false
                                                    : true
                                            this.setState({ disabled, data })
                                        }}
                                        required
                                    />
                                    <label
                                        htmlFor="terms_accepted"
                                        className={cssContact({
                                            smaller: true,
                                        })}>
                                        <span>
                                            Ich habe die{' '}
                                            <a
                                                href="https://www.candylabs.de/datenschutz"
                                                target="_blank">
                                                DATENSCHUTZ
                                            </a>{' '}
                                            Erklärung gelesen und stimme der
                                            Verarbeitung meiner
                                            personenbezogenen Daten und der
                                            Zusendung von Newslettern durch die
                                            Candylabs GmbH zu.
                                        </span>
                                    </label>
                                </div>

                                <div
                                    className={cssContact({
                                        'recaptcha-wrapper': true,
                                    })}>
                                    <Recaptcha
                                        sitekey="6Lc52XUUAAAAAGYuJOXdCWt6XwLb-8kwUYcuQg6C"
                                        render="explicit"
                                    />
                                </div>

                                <div>
                                    <Button
                                        primary="true"
                                        submit="true"
                                        disabled={this.state.disabled}>
                                        Anfrage senden
                                    </Button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className={cssContact({ details: true })}>
                        <div className={cssContact({ overlay: true })}>
                            <img src={DanielPutsche} />

                            <div>
                                <strong>Daniel Putsche</strong>
                            </div>
                            <div>Gründer | CEO</div>

                            <ul>
                                <li>
                                    <a
                                        className={cssContact({
                                            iconlink: true,
                                        })}
                                        href="mailto:hello@candylabs.de">
                                        <Mail /> hello@candylabs.de
                                    </a>
                                </li>
                                <li>
                                    <a
                                        className={cssContact({
                                            iconlink: true,
                                        })}
                                        href="tel:+49 69 348790 320">
                                        <Telephone /> +49 69 348790 320
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
