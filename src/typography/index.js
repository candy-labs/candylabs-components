import React from 'react'
import styles from './typography.scss'

export const H1 = props => <h1 {...props} className={styles.h1} />
export const H2 = props => <h2 {...props} className={styles.h2} />
export const H3 = props => <h3 {...props} className={styles.h3} />
export const Label = props => <span {...props} className={styles.label} />
export const P = props => <p {...props} className={styles.p} />
export const UL = props => (
    <ul className={styles.ul}>
        {props.items.map(item => {
            return <li key={item.id}>{item.text}</li>
        })}
    </ul>
)
export const OL = props => (
    <ol className={styles.ol}>
        {props.items.map(item => {
            return <li key={item.id}>{item.text}</li>
        })}
    </ol>
)
