import React from 'react'

export default () => (
    <svg
        width="19"
        height="19"
        viewBox="0 0 19 19"
        xmlns="http://www.w3.org/2000/svg">
        <g
            id="User-Interface"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="square">
            <g
                id="Menu"
                transform="translate(-1098 -465)"
                stroke="#000"
                strokeWidth="3">
                <g id="Group" transform="translate(631 442)">
                    <g id="ic_schliessen" transform="translate(469 25)">
                        <path
                            d="M0.241935484,0.241935484 L14.5181792,14.5181792"
                            id="Line"
                        />
                        <path
                            d="M0.241935484,0.241935484 L14.5181792,14.5181792"
                            id="Line"
                            transform="matrix(-1 0 0 1 15 0)"
                        />
                    </g>
                </g>
            </g>
        </g>
    </svg>
)
