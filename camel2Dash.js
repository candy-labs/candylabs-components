const kebab = str => {
  return str
    .replace(/[^a-zA-Z0-9]+/g, '-')
    .replace(/([A-Z]+)([A-Z][a-z])/g, '$1-$2')
    .replace(/([a-z])([A-Z])/g, '$1-$2')
    .replace(/([0-9])([^0-9])/g, '$1-$2')
    .replace(/([^0-9])([0-9])/g, '$1-$2')
    .replace(/-+/g, '-')
    .substring(1)
    .toLowerCase()
}

const convert = file => {
  const lineReader = require('readline').createInterface({
    input: require('fs').createReadStream(file),
  })
  
  lineReader.on('line', line => {
    const key = line.substring(0, line.indexOf(':'))
    const value = line.substring(line.indexOf(':'))
    if (key && value) {
      const kebabKey = kebab(key)
      console.log(`$${kebabKey}${value}`)
    }
  })
  return '// Auto generated file, do not modify'
}

module.exports.convert = convert
