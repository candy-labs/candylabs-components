echo "// Auto generated file, do not modify" > src/variables.js;
echo "export default " >> src/variables.js;
./node_modules/.bin/scss-to-json src/variables.scss >> src/variables.js;
node -p 'require("./camel2Dash").convert("src/variables.scss")' > src/variables_dash.scss;
sed -i -E 's/\$//' src/variables.js;
rm -f src/variables.js-E;
